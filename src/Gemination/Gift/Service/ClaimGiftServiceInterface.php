<?php

declare(strict_types=1);

namespace Gemination\Gift\Service;

/**
 * Интерфейс получения подарков
 */
interface ClaimGiftServiceInterface
{
    /**
     * Получает подарок
     *
     * @param int $giftId
     */
    public function claimGift(int $giftId);
}

<?php

declare(strict_types=1);

namespace Gemination\Gift\Service;

use Gemination\Gift\Model\Gift;

/**
 * Интерфейс сервиса списка подарков
 */
interface ListGiftServiceInterface
{
    /**
     * Возвращает список подарков пользователя
     *
     * @param string $userId
     *
     * @return Gift[]
     */
    public function getUserGifts(string $userId): array;
}

<?php

declare(strict_types=1);

namespace Gemination\Gift\Service;

use Gemination\Gift\Model\Gift;
use Gemination\Gift\Model\GiftInterface;
use Gemination\Gift\Repository\GiftRepositoryInterface;
use InvalidArgumentException;

/**
 * Интерфейс сервиса подарков
 */
class GiftService implements ListGiftServiceInterface, SendGiftServiceInterface, ClaimGiftServiceInterface
{
    /**
     * Репозиторий подарков
     *
     * @var GiftRepositoryInterface
     */
    private $giftRepository;

    /**
     * Конструктор
     *
     * @param GiftRepositoryInterface $giftRepository
     */
    public function __construct(GiftRepositoryInterface $giftRepository)
    {
        $this->giftRepository = $giftRepository;
    }

    /**
     * Возвращает список подарков пользователя
     *
     * @param string $userId
     *
     * @return GiftInterface[]
     */
    public function getUserGifts(string $userId): array
    {
        return $this->giftRepository->getUserGifts($userId);
    }

    /**
     * Отправляет подарок
     *
     * @param int $item
     * @param string $sender
     * @param string $recipient
     *
     * @return GiftInterface
     */
    public function sendGift(int $item, string $sender, string $recipient): GiftInterface
    {
        $gift = new Gift($item, $sender, $recipient);

        $gift->setId($this->giftRepository->addGift($gift));

        return $gift;
    }

    /**
     * Получает подарок
     *
     * @param int $giftId
     *
     * @return void
     */
    public function claimGift(int $giftId)
    {
        $gift = $this->giftRepository->getGiftById($giftId);

        if (!$gift) {
            throw new InvalidArgumentException('Gift with such id has not been found');
        }

        if ($gift->isClaimed()) {
            throw new Exception\GiftReclaimAttemptException('Gift has been claimed lately');
        }

        if ($gift->getSentAt()->getTimestamp() < strtotime('-7days')) {
            throw new Exception\OutdatedGiftException('Gift expiration date has been reached');
        }

        $gift->setIsClaimed(true);

        $this->giftRepository->markGiftClaimed($gift);

        // Здесь выполняет броски каких-нибудь событий и т.п
    }
}

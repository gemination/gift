<?php

declare(strict_types=1);

namespace Gemination\Gift\Service\Exception;

use DomainException;

/**
 * Исключение подарка, срок действия которого уже истек
 */
class OutdatedGiftException extends DomainException
{

}

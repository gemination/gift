<?php

declare(strict_types=1);

namespace Gemination\Gift\Service\Exception;

use DomainException;

/**
 * Исключение повторного получения подарка
 */
class GiftReclaimAttemptException extends DomainException
{

}

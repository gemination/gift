<?php

declare(strict_types=1);

namespace Gemination\Gift\Service;

use Gemination\Gift\Model\GiftInterface;

/**
 * Интерфейс сервиса отправки подарков
 */
interface SendGiftServiceInterface
{
    /**
     * Отправляет подарок
     *
     * @param int $item
     * @param string $sender
     * @param string $recipient
     *
     * @return GiftInterface
     */
    public function sendGift(int $item, string $sender, string $recipient): GiftInterface;
}

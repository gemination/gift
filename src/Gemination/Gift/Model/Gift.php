<?php

declare(strict_types=1);

namespace Gemination\Gift\Model;

use DateTimeImmutable;
use DateTimeInterface;

/**
 * Подарок
 */
class Gift implements GiftInterface
{
    /**
     * Идентификатор
     *
     * @var int
     */
    private $id;

    /**
     * Отправитель
     *
     * @var string
     */
    private $sender;

    /**
     * Получатель
     *
     * @var string
     */
    private $recipient;

    /**
     * Предмет подарка
     *
     * @var int
     */
    private $item;

    /**
     * @var DateTimeInterface
     */
    private $sentAt;

    /**
     * Индикация получения подарка
     *
     * @var bool
     */
    private $isClaimed;

    /**
     * Конструктор
     *
     * @param int $item
     * @param string $sender
     * @param string $recipient
     */
    public function __construct(int $item, string $sender, string $recipient)
    {
        $this->id        = 0;
        $this->item      = $item;
        $this->sender    = $sender;
        $this->recipient = $recipient;
        $this->sentAt    = new DateTimeImmutable();
        $this->isClaimed = false;
    }

    /**
     * Устанавливает идентификатор
     *
     * @internal запрещено использовать в целях бизнес-модели
     *
     * @param int $id
     * @return void
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * Возвращает идентификатор
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Возвращает отправителя
     *
     * @return string
     */
    public function getSender(): string
    {
        return $this->sender;
    }

    /**
     * Возвращает получаеля
     *
     * @return string
     */
    public function getRecipient(): string
    {
        return $this->recipient;
    }

    /**
     * Возвращает предмет
     *
     * @return int
     */
    public function getItem(): int
    {
        return $this->item;
    }

    /**
     * Указывает дату отправки
     *
     * @param DateTimeInterface $sentAt
     */
    public function setSentAt(DateTimeInterface $sentAt)
    {
        $this->sentAt = clone $sentAt;
    }

    /**
     * Возвращает дату отправки
     *
     * @return DateTimeInterface
     */
    public function getSentAt(): DateTimeInterface
    {
        return clone $this->sentAt;
    }

    /**
     * Проверяет, был ли получен подарок
     *
     * @return bool
     */
    public function isClaimed(): bool
    {
        return $this->isClaimed;
    }

    /**
     * Устанавливает индикацию получения подарка
     *
     * @param bool $isClaimed
     */
    public function setIsClaimed(bool $isClaimed)
    {
        $this->isClaimed = $isClaimed;
    }
}

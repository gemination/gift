<?php

declare(strict_types=1);

namespace Gemination\Gift\Model;

use DateTimeInterface;

/**
 * Интерфейс подарка
 */
interface GiftInterface
{
    /**
     * Возвращает идентификатор
     *
     * @return int
     */
    public function getId(): int;

    /**
     * Возвращает отправителя
     *
     * @return string
     */
    public function getSender(): string;

    /**
     * Возвращает получателя
     *
     * @return string
     */
    public function getRecipient(): string;

    /**
     * Возвращает сам предмет, являющийся подарком
     *
     * @return int
     */
    public function getItem(): int;

    /**
     * Возвращает дату отправки
     *
     * @return DateTimeInterface
     */
    public function getSentAt(): DateTimeInterface;

    /**
     * Проверяет, был ли получен подарок
     *
     * @return bool
     */
    public function isClaimed(): bool;
}

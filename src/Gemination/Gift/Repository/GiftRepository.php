<?php

declare(strict_types=1);

namespace Gemination\Gift\Repository;

use DateTime;
use Gemination\Gift\Model\Gift;
use Gemination\Gift\Model\GiftInterface;
use PDO;

/**
 * Репозиторий подарков
 */
class GiftRepository implements GiftRepositoryInterface
{
    /**
     * Подключение к бд
     *
     * @var PDO
     */
    private $connection;

    /**
     * Конструктор
     *
     * @param PDO $connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Добавляет подарок
     *
     * @param GiftInterface $gift
     *
     * @return int
     */
    public function addGift(GiftInterface $gift): int
    {
        $stmt = $this->connection->prepare(
            'INSERT INTO Gift(item, sender, recipient, sentAt) VALUE (:item, :sender, :recipient, :sentAt)'
        );

        $result = $stmt->execute([
            'item'      => $gift->getItem(),
            'sender'    => $gift->getSender(),
            'recipient' => $gift->getRecipient(),
            'sentAt'    => $gift->getSentAt()->format('Y-m-d H:i:s')
        ]);

        if (!$result) {
            throw new \RuntimeException(json_encode($this->connection->errorInfo()));
        }

        return (int) $this->connection->lastInsertId();
    }

    /**
     * Возвращает список подарков пользователя
     *
     * @param string $userId
     *
     * @return GiftInterface[]
     */
    public function getUserGifts(string $userId): array
    {
        $query = '
            SELECT id, item, sender, recipient, sentAt, isClaimed
            FROM Gift
            WHERE recipient=:recipient AND sentAt > NOW() - INTERVAL 7 DAY AND isClaimed=0  
            ORDER BY id DESC
        ';

        $stmt = $this->connection->prepare($query);
        $stmt->execute(['recipient' => $userId]);

        $userGifts = [];
        while ($giftData = $stmt->fetch()) {
            $userGifts[] = $this->hydrate($giftData);
        }

        return $userGifts;
    }

    /**
     * Получает последний отправленный подарок
     *
     * @param string $sender
     *
     * @return GiftInterface|null
     */
    public function getLastSentGift(string $sender): ?GiftInterface
    {
        $query = 'SELECT id, item, sender, recipient, sentAt, isClaimed FROM Gift WHERE sender=:sender ORDER BY id DESC';

        $stmt = $this->connection->prepare($query);
        $stmt->execute(['sender' => $sender]);

        $giftData = $stmt->fetch();

        if (!$giftData) {
            return null;
        }

        return $this->hydrate($giftData);
    }

    /**
     * Возвращает подарок по идентификатору
     *
     * @param int $giftId
     *
     * @return Gift|null
     */
    public function getGiftById(int $giftId): ?Gift
    {
        $stmt = $this->connection->prepare('SELECT id, item, sender, recipient, sentAt, isClaimed FROM Gift WHERE id=:id');
        $stmt->execute(['id' => $giftId]);

        $giftData = $stmt->fetch();
        if (!$giftData) {
            return null;
        }

        return $this->hydrate($giftData);
    }

    /**
     * Отмечает подарок, как полученный
     *
     * @param GiftInterface $gift
     *
     * @return void
     */
    public function markGiftClaimed(GiftInterface $gift)
    {
        $stmt = $this->connection->prepare('UPDATE Gift SET isClaimed=1 WHERE id=:id');
        $stmt->execute(['id' => $gift->getId()]);
    }

    /**
     * Гидрирует данные в подарок
     *
     * @param array $giftData
     *
     * @return Gift
     */
    private function hydrate(array $giftData): Gift
    {
        $gift = new Gift(
            (int) $giftData['item'],
            $giftData['sender'],
            $giftData['recipient']
        );
        $gift->setId((int)$giftData['id']);
        $gift->setIsClaimed((bool) $giftData['isClaimed']);
        $gift->setSentAt(new DateTime($giftData['sentAt']));

        return $gift;
    }
}

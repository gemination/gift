<?php

declare(strict_types=1);

namespace Gemination\Gift\Repository;

use Gemination\Gift\Model\Gift;
use Gemination\Gift\Model\GiftInterface;

/**
 * Интерфейс репозитория подарков
 */
interface GiftRepositoryInterface
{
    /**
     * Добавляет подарок
     *
     * @param GiftInterface $gift
     *
     * @return int
     */
    public function addGift(GiftInterface $gift): int;

    /**
     * Отмечает подарок, как полученный
     *
     * @param GiftInterface $gift
     *
     * @return void
     */
    public function markGiftClaimed(GiftInterface $gift);

    /**
     * Возвращает список подарков пользователя
     *
     * @param string $userId
     *
     * @return GiftInterface[]
     */
    public function getUserGifts(string $userId): array;

    /**
     * Возвращает последний отправленный подарок
     *
     * @param string $sender
     *
     * @return GiftInterface|null
     */
    public function getLastSentGift(string $sender): ?GiftInterface;

    /**
     * Возвращает подарок по идентификатору
     *
     * @param int $giftId
     *
     * @return Gift|null
     */
    public function getGiftById(int $giftId): ?Gift;
}

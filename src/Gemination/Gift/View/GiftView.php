<?php

declare(strict_types=1);

namespace Gemination\Gift\View;

use Gemination\Gift\Model\GiftInterface;

/**
 * Представление подарка
 */
class GiftView
{
    /**
     * Возвращает представление
     *
     * @param GiftInterface $gift
     *
     * @return array
     */
    public static function getView(GiftInterface $gift): array
    {
        return [
            'id'        => $gift->getId(),
            'friend_id' => $gift->getSender(),
            'gift_id'   => $gift->getItem(),
        ];
    }
}

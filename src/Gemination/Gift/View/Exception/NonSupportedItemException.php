<?php

declare(strict_types=1);

namespace Gemination\Gift\View\Exception;

use InvalidArgumentException;

/**
 * Исключение, неподдерживаемого представлением, элемента
 */
class NonSupportedItemException extends InvalidArgumentException
{

}

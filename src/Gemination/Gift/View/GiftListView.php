<?php

declare(strict_types=1);

namespace Gemination\Gift\View;

use Gemination\Gift\Model\GiftInterface;

/**
 * Представление списка подарков
 */
class GiftListView
{
    /**
     * Возвращает представление
     *
     * @param array $gifts
     *
     * @return array
     */
    public static function getView(array $gifts): array
    {
        $view = [];

        foreach ($gifts as $gift) {
            if (!self::isSupportedItem($gift)) {
                throw new Exception\NonSupportedItemException('Представление переданного элемента не поддерживается');
            }

            $view[] = GiftView::getView($gift);
        }

        return $view;
    }

    /**
     * Проверяет поддержку представления элемента
     *
     * @param mixed $item
     *
     * @return bool
     */
    private static function isSupportedItem($item): bool
    {
        if (!$item instanceof GiftInterface) {
            return false;
        }

        return true;
    }
}

<?php

declare(strict_types=1);

namespace Gemination\Gift\Controller;

use Gemination\Gift\Repository\GiftRepositoryInterface;
use Gemination\Gift\Service\ClaimGiftServiceInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Контроллер получения подарков
 */
class ClaimGiftController extends AbstractController
{
    /**
     * Сервис получения подарков
     *
     * @var ClaimGiftServiceInterface
     */
    private $claimGiftService;

    /**
     * Репозиторий подарков
     *
     * @var GiftRepositoryInterface
     */
    private $giftRepository;

    /**
     * Конструктор
     *
     * @param ClaimGiftServiceInterface $claimGiftService
     * @param GiftRepositoryInterface $giftRepository
     */
    public function __construct(ClaimGiftServiceInterface $claimGiftService, GiftRepositoryInterface $giftRepository)
    {
        $this->claimGiftService = $claimGiftService;
        $this->giftRepository   = $giftRepository;
    }

    /**
     * Получает подарок
     *
     * @param string  $userId  Бесполезный параметр с точки зрения процедуры и RESTful
     * @param Request $request
     *
     * @return Response
     */
    public function execute(string $userId, Request $request): Response
    {
        $currentUser = $this->getCurrentUser();

        if ($currentUser === null) {
            return $this->error(401, 'Unauthorized');
        }

        $giftId = $request->request->getInt('id');

        if (!$giftId) {
            return $this->error(400, 'No gift identifier provided');
        }

        $gift = $this->giftRepository->getGiftById($giftId);
        if (!$gift) {
            return $this->error(404, 'Gift has not been found');
        }

        // Проверка тупо для проформы. Она лишена смысла, так как в подарке уже указан получатель
        if ($userId  !== $currentUser) {
            return $this->error(403, 'Gift belongs to another user');
        }

        if ($gift->getRecipient() !== $currentUser) {
            return $this->error(403, 'Gift belongs to another user');
        }

        $this->claimGiftService->claimGift($giftId);

        return $this->respond(0);
    }
}

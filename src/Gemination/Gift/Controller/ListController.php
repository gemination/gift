<?php

declare(strict_types=1);

namespace Gemination\Gift\Controller;

use Gemination\Gift\Service\ListGiftServiceInterface;
use Gemination\Gift\View\GiftListView;
use Symfony\Component\HttpFoundation\Response;

/**
 * Контроллер получения списка подарков пользователя
 */
class ListController extends AbstractController
{
    /**
     * Сервис подарков пользователя
     *
     * @var ListGiftServiceInterface
     */
    private $giftListService;

    /**
     * Конструктор
     *
     * @param ListGiftServiceInterface $giftListService
     */
    public function __construct(ListGiftServiceInterface $giftListService)
    {
        $this->giftListService = $giftListService;
    }

    /**
     * Возвращает список подарков
     *
     * @param string $userId
     *
     * @return Response
     */
    public function execute(string $userId): Response
    {
        $currentUser = $this->getCurrentUser();

        if ($currentUser === null) {
            return $this->error(401, 'Unauthorized');
        }

        if ($currentUser !== $userId) {
            return $this->error(403, 'Listing other users gifts is forbidden');
        }

        $gifts = $this->giftListService->getUserGifts($userId);

        return $this->respond(0, GiftListView::getView($gifts));
    }
}

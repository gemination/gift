<?php

declare(strict_types=1);

namespace Gemination\Gift\Controller;

use Gemination\Gift\Repository\GiftRepositoryInterface;
use Gemination\Gift\Service\SendGiftServiceInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Контроллер отправки подарков друзьям
 */
class SendGiftController extends AbstractController
{
    /**
     * Сервис отправки подарков
     *
     * @var SendGiftServiceInterface
     */
    private $sendGiftService;

    /**
     * Репозиторий подарков
     *
     * @var GiftRepositoryInterface
     */
    private $giftRepository;

    /**
     * Конструктор
     *
     * @param GiftRepositoryInterface $giftRepository
     * @param SendGiftServiceInterface $sendGiftService
     */
    public function __construct(SendGiftServiceInterface $sendGiftService, GiftRepositoryInterface $giftRepository)
    {
        $this->sendGiftService = $sendGiftService;
        $this->giftRepository  = $giftRepository;
    }

    /**
     * Отправляет подарок другу
     *
     * @param string $friendId
     * @param Request $request
     *
     * @return Response
     */
    public function execute(string $friendId, Request $request): Response
    {
        $currentUser = $this->getCurrentUser();

        if ($currentUser === null) {
            return $this->error(401, 'Unauthorized');
        }

        if (!$this->canSendGiftTo($friendId)) {
            return $this->error(403, 'Can not send gift');
        }

        if (!$request->request->has('gift_id')) {
            return $this->error(404, 'Gift information is missing in request');
        }

        $giftId = $request->request->getInt('gift_id');
        $this->sendGiftService->sendGift($giftId, $currentUser, $friendId);

        return $this->respond(0);
    }

    /**
     * Проверяет возможность отправки подарка получателю
     *
     * @TODO выполняет роль сервиса прав доступов, нарушая SRP
     *
     * @param string $recipient
     *
     * @return bool
     */
    private function canSendGiftTo(string $recipient): bool
    {
        $currentUser = $this->getCurrentUser();

        // Предполагаем, что самому себе слать подарки нельзя
        if ($currentUser === $recipient) {
            return false;
        }

        $lastSentGift = $this->giftRepository->getLastSentGift($this->getCurrentUser());
        if (!$lastSentGift) {
            return true;
        }

        if ($lastSentGift->getSentAt()->getTimestamp() < strtotime('-1day')) {
            return true;
        }

        return false;
    }
}

<?php

declare(strict_types=1);

namespace Gemination\Gift\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Абстрактный контроллер, имитирующий некоторые из процессов слоя приложения
 */
abstract class AbstractController
{
    /**
     * Возвращает идентификатор текущего пользователя
     *
     * @return null|string
     */
    protected function getCurrentUser(): ?string
    {
        // Да, будем хранить здесь ид пользователя. Сказали, аутентификация не нужна
        if (!isset($_GET['ex_machina'])) {
            return null;
        }

        // Банальная проверка, чтобы мы знали, с чем имеем дело
        if (mb_strlen($_GET['ex_machina']) !== 36) {
            return null;
        }

        return $_GET['ex_machina'];
    }

    /**
     * Возвращает ответ
     *
     * @param int        $status
     * @param array|null $data
     *
     * @return Response
     */
    protected function respond(int $status, array $data = null): Response
    {
        $responseContent = ['status' => $status];

        if ($data !== null) {
            $responseContent['data'] = $data;
        }

        return new JsonResponse($responseContent);
    }

    /**
     * Возвращает ответ об ошибке исполнения
     *
     * @param int    $httpCode
     * @param string $message
     *
     * @return Response
     */
    protected function error(int $httpCode, string $message): Response
    {
        return new JsonResponse(['status' => $httpCode, 'message' => $message], $httpCode);
    }
}

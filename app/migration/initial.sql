CREATE TABLE Gift (
  id INT UNSIGNED AUTO_INCREMENT NOT NULL,
  item INT UNSIGNED NOT NULL,
  sender VARCHAR(36) NOT NULL,
  recipient VARCHAR(36) NOT NULL,
  sentAt DATETIME NOT NULL,
  isClaimed BOOLEAN DEFAULT 0,
  INDEX received_gifts_idx (recipient, sentAt, isClaimed),
  INDEX sender_last_gifts_idx (sender, sentAt, isClaimed),
  PRIMARY KEY(id)
)
DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;

<?php

declare(strict_types=1);

$config = include __DIR__.'/../config/database.php';

$connection = new PDO(
    $config['dsn'],
    $config['username'],
    $config['password']
);

$connection->beginTransaction();
$connection->exec(file_get_contents(__DIR__.'/initial.sql'));
$connection->commit();

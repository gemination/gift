<?php

declare(strict_types=1);

namespace App;

use Psr\Container\ContainerInterface;
use ReflectionMethod;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Throwable;

class Kernel
{
    private $configDir;

    /**
     * @var ContainerInterface
     */
    private $container;

    private $routeMatcher;

    private function __construct()
    {
        $this->configDir = __DIR__ . '/config/';

        $this->routeMatcher = new UrlMatcher(require ($this->configDir . '/routes.php'), new RequestContext());
        $this->container    = require ($this->configDir .'/classmap.php');
    }

    public static function init()
    {
        return new self;
    }

    public function handle(Request $request): Response
    {
        try {
            $routeInfo = $this->routeMatcher->matchRequest($request);
        } catch (Throwable $e) {
            $response = new JsonResponse(['message' => 'No route found'], JsonResponse::HTTP_NOT_FOUND);
            $response->prepare($request);

            return $response;
        }

        try {
            $routeInfo['request'] = $request;
            $response = $this->executeController($routeInfo);
        } catch (Throwable $error) {
            $response = new JsonResponse(
                ['message' => $error->getMessage()],
                in_array($error->getCode(), [400, 401, 403, 404], true) ? $error->getCode() : 500
            );
        }

        $response->prepare($request);

        return $response;
    }

    private function executeController($requestInfo)
    {
        $controller = $this->container->get($requestInfo['controller']);

        $arguments = [];
        $executionMethod = new ReflectionMethod($controller, 'execute');
        foreach ($executionMethod->getParameters() as $expectedParameter) {
            if (array_key_exists($expectedParameter->getName(), $requestInfo)) {
                $arguments[] = $requestInfo[$expectedParameter->getName()];
            } else {
                if ($expectedParameter->isDefaultValueAvailable()) {
                    $arguments[] = $expectedParameter->getDefaultValue();
                }
            }
        }

        return call_user_func_array([$controller, 'execute'], $arguments);
    }
}

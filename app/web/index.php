<?php

declare(strict_types = 1);

define('APP_DIR', __DIR__ . '/..');
define('CONFIG_DIR', APP_DIR .'/config');

use Symfony\Component\HttpFoundation\Request;

require_once APP_DIR . '/../vendor/autoload.php';

$app = \App\Kernel::init();

$request  = Request::createFromGlobals();
$response = $app->handle($request);
$response->send();

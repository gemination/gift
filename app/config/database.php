<?php

// Всё в docker-compose
return [
    'dsn'      => 'mysql:dbname=game_db;host=db',
    'username' => 'game_user',
    'password' => 'game_pass',
];

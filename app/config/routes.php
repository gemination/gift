<?php

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

$rawRoutes = [
    'gift.list' => [
        'path'         => '/gifts/{userId}',
        'methods'      => ['GET'],
        'requirements' => ['userId' => '[A-Za-z0-9-]{36}'],
        'controller'   => 'gift.controller.list',
    ],
    'gift.send' => [
        'path'         => '/gifts/{friendId}',
        'methods'      => ['POST'],
        'requirements' => ['friendId' => '[A-Za-z0-9-]{36}'],
        'controller'   => 'gift.controller.send',
    ],
    'gift.claim' => [
        'path'         => '/gifts/{userId}',
        'methods'      => ['PUT'],
        'requirements' => ['userId' => '[A-Za-z0-9-]{36}'],
        'controller'   => 'gift.controller.claim',
    ],
];

$routes = new RouteCollection();

foreach ($rawRoutes as $routeName => $routeInfo) {
    $routes->add(
        $routeName,
        new Route(
            $routeInfo['path'],
            ['controller' => $routeInfo['controller']],
            $routeInfo['requirements'],
            [],
            null,
            [],
            $routeInfo['methods']
        )
    );
}

return $routes;

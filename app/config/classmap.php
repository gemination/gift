<?php

$container = new DI\Container();

// Инфраструктурщина
$dbConfig = include __DIR__ . '/database.php';
$container->set('database', new PDO($dbConfig['dsn'], $dbConfig['username'], $dbConfig['password']));

// Репозитории
$container->set('gift.repository.gift', DI\create(Gemination\Gift\Repository\GiftRepository::class)->constructor(DI\get('database')));

// Сервисы
$giftServiceDefinition = DI\create(\Gemination\Gift\Service\GiftService::class)->constructor(DI\get('gift.repository.gift'));
$container->set('gift.service.list_gifts', $giftServiceDefinition);
$container->set('gift.service.send_gift', $giftServiceDefinition);
$container->set('gift.service.claim_gift', $giftServiceDefinition);

// Контроллеры
$container->set('gift.controller.list', DI\create(\Gemination\Gift\Controller\ListController::class)
    ->constructor(DI\get('gift.service.list_gifts')));
$container->set('gift.controller.send', DI\create(\Gemination\Gift\Controller\SendGiftController::class)
    ->constructor(DI\get('gift.service.send_gift'), DI\get('gift.repository.gift')));
$container->set('gift.controller.claim', DI\create(\Gemination\Gift\Controller\ClaimGiftController::class)
    ->constructor(DI\get('gift.service.claim_gift'), DI\get('gift.repository.gift')));

return $container;

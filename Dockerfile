FROM php:7.2-fpm

RUN apt-get update && apt-get install -y \
    apt-utils \
    zip \
    unzip \
    ssh \
    g++ \
    git \
    curl \
    libcurl4-gnutls-dev \
    libpq-dev \
    libicu-dev \
    mysql-client

RUN docker-php-ext-install \
    intl \
    curl \
    bcmath \
    gettext \
    mbstring \
    pdo_mysql

RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/ \
    && ln -s /usr/local/bin/composer.phar /usr/local/bin/composer

WORKDIR /var/www/html

COPY . ./

RUN composer install --no-interaction
